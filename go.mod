module bitbucket.org/JakeKalstad/core

go 1.17

require (
	github.com/dpapathanasiou/go-recaptcha v0.0.0-20190121160230-be5090b17804
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gofrs/uuid v4.1.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.4
	github.com/tdewolff/minify v2.3.6+incompatible
)

require (
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.17.0 // indirect
	github.com/tdewolff/parse v2.3.4+incompatible // indirect
	github.com/tdewolff/test v1.0.6 // indirect
)
