# Core Lib

## Must Use config.json file or supply environments

        "db": true,
        "redis": true,
        "port": "9123",
        "captcha": "xyz-sdfz-z-dsf",
        "analytics_password": "",
        "privacy_policy": "privacy.html"
    }

`DASHBOARD_KEY=XYZ DB=true REDIS=true Port=PORT PRIVACY_POLICY="true"`

## If DB - Required Environment Variables

`DB_HOST=localhost DB_NAME=my_db DB_MODE=disable DB_USER="userabc DB_PASSWORD="pass DB_ROOT="" DB_CLIENTKEY="" DB_CERT=""` go run main.go

## If Redis - Required Environment Variables

`DB_HOST=localhost DB_NAME=my_db DB_MODE=disable DB_USER="userabc DB_PASSWORD="pass DB_ROOT="" DB_CLIENTKEY="" DB_CERT=""` go run main.go

## If Captcha - Required Environment Variables

`CAPTCHA=6LfFRroaAAAAADDd5j9PdSLbdD7xCl_ItEil8UiD` go run main.go

# If analytics - Required Environment Variables

`DASHBOARD_KEY="abc"` go run main.go

# USAGE

    type App struct
        router    *mux.Router
        Analytics Analyzer
        DB        *sql.DB
        Redis     *redis.Client
    }

    func test()
        app := &App{}
        Initialize(&InitializeData{
            GetRedis: func(c *redis.Client)
                app.Redis = c
            },
            GetConnection: func(d *sql.DB)
                app.DB = d
            },
            CustomRoutes: func(r mux.Router)
                router.HandleFunc("/how-to-move-to-puerto-rico/", app.howTo).Methods("GET")
                router.HandleFunc("/getting-electricity-in-san-juan-puerto-rico/", app.howToFindElectricity).Methods("GET")
                router.HandleFunc("/getting-water-in-san-juan-puerto-rico/", app.howToFindWater).Methods("GET")
                router.HandleFunc("/getting-internet-in-san-juan-puerto-rico/", app.howToFindInternet).Methods("GET")
                router.HandleFunc("/getting-mobile-in-san-juan-puerto-rico/", app.howToFindMobile).Methods("GET")
            },
        })
    }

# API

analytics.go:func URLGroupBy1SumBy2(URL string) (groupPart, entryPart string)

analytics.go:func urlDataDefault(URL string) (groupPart, entryPart string) 

analytics.go:func NewAnalytics(name string, URLData func(URL string) (groupPart, entryPart string)) Analyzer 

analytics.go:func (a analytics) ScheduleWrite(delayMinutes int)

analytics.go:func (a analytics) InsertRequest(r \_http.Request)

analytics.go:func (a analytics) Dashboard(w http.ResponseWriter, r \_http.Request)

analytics.go:func buildTemplate(templateName string) (\_template.Template, error)

analytics.go:func (a analytics) readSavedData(td time.Time) map[string][]action

analytics.go:func (a analytics) insert(ip string, act action)

analytics.go:func (a analytics) get() map[string]map[string][]action

analytics.go:func (a analytics) getByIP(ip string) \[]action

analytics.go:func (a analytics) writeFile() error

core.go:func GetUUID() uuid.UUID

core.go:func Env(key, def string) string

core.go:func GetConnection() (\_sql.DB, error)

core.go:func Initialize(initData \_InitializeData)

core.go:func (w GzipResponseWriter) Write(b \[]byte) (int, error)

core.go:func ToNullString(s string) sql.NullString

core.go:func StringToNullStringMap(m map[string]string) hstore.Hstore

core.go:func NullStringToStringMap(m hstore.Hstore) map[string]string

core.go:func ReadUserIP(r \_http.Request) string

core.go:func HashPassword(password string) [64]byte

core.go:func CheckPasswordHash(password, hash string) bool

core.go:func MakeGzipHandler(h http.Handler) http.Handler

core.go:func privacy(w http.ResponseWriter, r \_http.Request)

core.go:func NotFound(w http.ResponseWriter, r \_http.Request)

core.go:func WellKnownHandler(w http.ResponseWriter, r \_http.Request)

core.go:func FileHandler(w http.ResponseWriter, r \_http.Request)

core.go:func MaxAgeHandler(seconds int, h http.Handler) http.Handler

core.go:func processRequest(w http.ResponseWriter, r \_http.Request)

core.go:func (wr \_dumWriter) Header() http.Header        return nil }

core.go:func (wr \_dumWriter) Write(\[]byte) (int, error)  return 0, nil }

core.go:func (wr \_dumWriter) WriteHeader(statusCode int)}

core.go:func writeError(w http.ResponseWriter, msg string, status int)

core.go:func writeTemplate(t \_template.Template, w http.ResponseWriter, data interface{})

core.go:func Marshal(v interface{}) template.JS

core.go:func unrollStrings(strs \[]string) string

core.go:func makeRange(min, max int) \[]int

core.go:func getPhoneHREF(phone string) string

core.go:func URLEncodeName(name string) string

core.go:func dateFromYear(year int) string
