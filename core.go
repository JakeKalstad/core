package core

import (
	"compress/gzip"
	"crypto/sha512"
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dpapathanasiou/go-recaptcha"
	"github.com/go-redis/redis"
	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"github.com/lib/pq/hstore"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/css"
	"github.com/tdewolff/minify/html"
	"github.com/tdewolff/minify/svg"
)

const STATIC_DIR = "/static/"
const TEMPLATE = "layouts/layout.html"

func GetUUID() uuid.UUID {
	newKey, _ := uuid.NewV4()
	return newKey
}

func Env(key, def string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return def
}

func GetConnection() (*sql.DB, error) {
	db, err := sql.Open("postgres", fmt.Sprintf("host=%s dbname=%s sslmode=%s user=%s password=%s sslrootcert=%s sslkey=%s sslcert=%s",
		Env("DB_HOST", "localhost"),
		Env("DB_NAME", "sjpr_dev"),
		Env("DB_MODE", "disable"),
		Env("DB_USER", "sjpr"),
		Env("DB_PASSWORD", "2021sjpr"),
		Env("DB_ROOT", "root.crt"),
		Env("DB_CLIENTKEY", "client.key"),
		Env("DB_CERT", "client.crt"),
	))
	if err != nil {

		return nil, err
	}
	db.SetMaxOpenConns(100)
	db.SetMaxIdleConns(100)
	err = db.Ping()
	return db, err
}

type InitializeData struct {
	GetConnection func(*sql.DB)
	CustomRoutes  func(*mux.Router)
	GetRedis      func(*redis.Client)
	GetName       func(string)
	GetDomain     func(string)
}

type ConfigData struct {
	Name               string
	Domain             string
	DB                 bool
	Redis              bool
	Captcha            string
	Port               string
	AnaylyticsPassword string
	PrivacyPolicy      bool
}

func Initialize(initData *InitializeData) {
	environmentExists := os.Getenv("ENV_HOST")
	cfd := &ConfigData{}
	if len(environmentExists) > 0 {
		cfd.AnaylyticsPassword = os.Getenv("DASHBOARD_KEY")
		cfd.DB = os.Getenv("DB") == "true"
		cfd.Redis = os.Getenv("REDIS") == "true"
		cfd.Captcha = os.Getenv("CAPTCHA")
		cfd.Port = os.Getenv("PORT")
		cfd.PrivacyPolicy = os.Getenv("PRIVACY_POLICY") == "true"
	} else {
		bits, err := ioutil.ReadFile("config.json")
		if err != nil {
			panic(`Must supply a config.json - example:::
			{
				"db": true,
				"redis": true,
				"port": "9123",
				"captcha": true,
				"analytics_password": "",
				"privacy_policy": "privacy.html"
			}
			`)
		}
		err = json.Unmarshal(bits, cfd)
		if err != nil {
			panic(`Malformed config.json`)
		}
	}
	initData.GetName(cfd.Name)
	initData.GetDomain(cfd.Domain)
	router := mux.NewRouter().StrictSlash(true)
	m := minify.New()
	m.AddFunc("text/css", css.Minify)
	m.AddFunc("text/html", html.Minify)
	m.AddFunc("image/svg+xml", svg.Minify)

	router.HandleFunc("/robots.txt", FileHandler).Methods("GET")
	router.HandleFunc("/ads.txt", FileHandler).Methods("GET")
	router.HandleFunc("/sitemap.xml", FileHandler).Methods("GET")
	router.
		PathPrefix("/static/").
		Handler(http.StripPrefix(STATIC_DIR, m.Middleware(http.FileServer(http.Dir("."+STATIC_DIR)))))
	if len(cfd.Captcha) > 0 {
		recaptcha.Init(cfd.Captcha)
	}
	if cfd.Redis {
		client := redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "",
			DB:       0,
		})
		_, err := client.Ping().Result()
		if err != nil {
			panic(err)
		}
		initData.GetRedis(client)
	}
	if cfd.DB {
		db, err := GetConnection()
		if err != nil {
			panic(err)
		}
		initData.GetConnection(db)
		defer db.Close()
	}
	if len(cfd.AnaylyticsPassword) > 0 {
		analytics := NewAnalytics(cfd.Name, URLGroupBy1SumBy2)
		router.HandleFunc("/analytics", analytics.Dashboard).Methods("GET")
		analytics.ScheduleWrite(1)
	}
	router.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
			w.Header().Set("Last-Modified", time.Now().Format(http.TimeFormat))
			w.Header().Set("Pragma", "no-cache")
			w.Header().Set("Expires", "Fri, 01 Jan 1990 00:00:00 GMT")

			next.ServeHTTP(w, r)
		})
	})
	// ================================
	if cfd.PrivacyPolicy {
		router.HandleFunc("/privacy/", privacy).Methods("GET")
	}
	initData.CustomRoutes(router)

	router.HandleFunc("/captcha/{token}", processRequest).Methods("POST")
	router.NotFoundHandler = http.HandlerFunc(NotFound)

	log.Fatal(http.ListenAndServe(":"+cfd.Port, router))
}

type GzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w GzipResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

type Basic struct {
	Title   string
	Content string
}

func ToNullString(s string) sql.NullString {
	return sql.NullString{String: s, Valid: s != ""}
}
func StringToNullStringMap(m map[string]string) hstore.Hstore {
	nm := map[string]sql.NullString{}
	for k, v := range m {
		nm[k] = ToNullString(v)
	}
	return hstore.Hstore{
		Map: nm,
	}
}

func NullStringToStringMap(m hstore.Hstore) map[string]string {
	nm := map[string]string{}
	for k, v := range m.Map {
		nm[k] = v.String
	}
	return nm
}

func ReadUserIP(r *http.Request) string {
	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}
	addys := strings.Split(IPAddress, ":")
	return addys[0]
}

func HashPassword(password string) [64]byte {
	return sha512.Sum512([]byte(password))
}

func CheckPasswordHash(password, hash string) bool {
	bits := sha512.Sum512([]byte(password))
	return fmt.Sprintf("%x", bits) == hash
}

func MakeGzipHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			h.ServeHTTP(w, r)
			return
		}
		w.Header().Set("Content-Encoding", "gzip")
		gz := gzip.NewWriter(w)
		defer gz.Close()
		gzr := GzipResponseWriter{Writer: gz, ResponseWriter: w}
		h.ServeHTTP(gzr, r)
	})
}

func privacy(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles(TEMPLATE, "content/privacy.html")
	t.ExecuteTemplate(w, "layout", &Basic{Title: "Privacy",
		Content: "privacy",
	})
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles(TEMPLATE, "content/custom_404.html")
	w.WriteHeader(http.StatusNotFound)
	t.ExecuteTemplate(w, "layout", &Basic{Title: "404", Content: "Page Not Found"})
}

func WellKnownHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "."+STATIC_DIR+r.URL.Path[13:])
}

func FileHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "."+STATIC_DIR+r.URL.Path[1:])
}

func MaxAgeHandler(seconds int, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", fmt.Sprintf("max-age=%d, public, immutable", seconds))
		h.ServeHTTP(w, r)
	})
}

const MAX_UPLOAD_SIZE = (1024 * 1024) * 8

func UploadFiles(r *http.Request, business, imgType string) ([]string, error) {
	files := r.MultipartForm.File["images"]
	names := []string{}
	for _, fileHeader := range files {
		if fileHeader.Size > MAX_UPLOAD_SIZE {
			return nil, fmt.Errorf("The uploaded image is too big: %s. Please use an image less than 1MB in size", fileHeader.Filename)
		}
		file, err := fileHeader.Open()
		if err != nil {
			return nil, err
		}
		defer file.Close()

		buff := make([]byte, 512)
		_, err = file.Read(buff)
		if err != nil {
			return nil, err
		}

		filetype := http.DetectContentType(buff)
		if filetype != "image/jpeg" && filetype != "image/png" {
			return nil, fmt.Errorf("The provided file format is not allowed. Please upload a JPEG or PNG image")
		}

		_, err = file.Seek(0, io.SeekStart)
		if err != nil {
			return nil, err
		}
		directory := fmt.Sprintf("/static/img/%s/%s/", imgType, business)
		err = os.MkdirAll("."+directory, os.ModePerm)
		if err != nil {
			return nil, err
		}
		fileName := fmt.Sprintf("%s%s", directory, fileHeader.Filename)
		f, err := os.Create("." + fileName)
		if err != nil {
			return nil, err
		}

		defer f.Close()

		_, err = io.Copy(f, file)
		if err != nil {
			return nil, err
		}
		names = append(names, fileName)
	}
	return names, nil
}

func processRequest(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	token := vars["token"]
	if len(token) > 0 {
		result, err := recaptcha.Confirm("127.0.0.1", token)
		if err != nil {
			WriteError(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write([]byte(fmt.Sprintf("%t", result)))
		return
	}
	w.Write([]byte(fmt.Sprintf("%t", false)))
}

type dumWriter struct{}

func (wr *dumWriter) Header() http.Header        { return nil }
func (wr *dumWriter) Write([]byte) (int, error)  { return 0, nil }
func (wr *dumWriter) WriteHeader(statusCode int) {}

func WriteError(w http.ResponseWriter, msg string, status int) {
	fmt.Println(msg)
	w.WriteHeader(status)
	w.Write(nil)
}

func WriteTemplate(t *template.Template, w http.ResponseWriter, data interface{}) {
	err := t.ExecuteTemplate(w, "layout", data)
	if err != nil {
		WriteError(&dumWriter{}, err.Error(), http.StatusInternalServerError)
		return
	}
}

// Func Map

func Marshal(v interface{}) template.JS {
	a, err := json.Marshal(v)
	if err != nil {
		fmt.Println(err)
	}
	return template.JS(a)
}

func UnrollStrings(strs []string) string {
	return strings.Join(strs, ",")
}

func MakeRange(min, max int) []int {
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}

func GetPhoneHREF(phone string) string {
	phone = strings.ReplaceAll(phone, "(", "")
	phone = strings.ReplaceAll(phone, ")", "")
	phone = strings.ReplaceAll(phone, "-", "")
	phone = strings.ReplaceAll(phone, " ", "")
	return phone
}

func URLEncodeName(name string) string {
	return strings.Replace(name, " ", "-", -1)
}

func DateFromYear(year int) string {
	n := time.Now()
	if year > 0 {
		n = n.Add(time.Duration((365 * year)) * (24 * time.Hour))
	}
	return n.Format("2006-01-02")
}

func Test() {

}
