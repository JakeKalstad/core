package core

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

type Analyzer interface {
	ScheduleWrite(delayMinutes int)
	Dashboard(w http.ResponseWriter, r *http.Request)
	InsertRequest(r *http.Request)
}

func URLGroupBy1SumBy2(URL string) (groupPart, entryPart string) {
	pParts := strings.Split(URL, "/")
	catKey := pParts[1]
	entityKey := strings.Join(pParts[2:], "/")
	return catKey, entityKey
}

func urlDataDefault(URL string) (groupPart, entryPart string) {
	return "", URL
}

func NewAnalytics(name string, URLData func(URL string) (groupPart, entryPart string)) Analyzer {
	if URLData == nil {
		URLData = urlDataDefault
	}
	ana := &analytics{
		Name:     name,
		Password: os.Getenv("DASHBOARD_KEY"),
		URLData:  URLData,
		Mux:      &sync.RWMutex{},
	}
	ana.IPEntries = map[string]map[string][]action{}
	ana.IPEntries[time.Now().Local().Format("2006-01-02")] = ana.readSavedData(time.Now().Local())
	return ana
}

func (a analytics) ScheduleWrite(delayMinutes int) {
	ticker := time.NewTicker(time.Duration(delayMinutes) * time.Minute)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				a.writeFile()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}

func (a analytics) InsertRequest(r *http.Request) {
	ua := strings.ToLower(r.UserAgent())
	bots := []string{"wget", "python", "perl", "msnbot", "php", "bot", "archive", "crawl"}
	for _, b := range bots {
		if strings.Contains(strings.ToLower(ua), b) {
			return
		}
	}
	act := action{Page: r.URL.Path, Query: r.URL.RawQuery}
	a.Mux.Lock()
	defer a.Mux.Unlock()
	a.insert(r.RemoteAddr, act)
}

func (a analytics) Dashboard(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	if len(a.Password) > 0 && (len(q["k"]) == 0 || len(q["k"][0]) == 0 || q["k"][0] != a.Password) {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write(nil)
		return
	}

	date := time.Now()
	var err error
	if len(q["date"]) > 0 {
		date, err = time.Parse("2006-01-02", q["date"][0])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write(nil)
			return
		}
	}
	var data map[string][]action
	if date.Format("2006-01-02") == time.Now().Format("2006-01-02") {
		data = a.IPEntries[date.Format("2006-01-02")]
	} else {
		data = a.readSavedData(date)
	}

	entries := len(data)
	urlHits := map[string]map[string]int{}
	for _, actions := range data {
		for _, act := range actions {
			groupBy, dataEntry := a.URLData(act.Page)
			_, ok := urlHits[groupBy]
			if !ok {
				urlHits[groupBy] = map[string]int{}
			}

			urlHits[groupBy][dataEntry] = urlHits[groupBy][dataEntry] + 1
		}
	}

	dd := dashData{SessionCount: entries, URLHits: urlHits, Date: date.Format("2006-01-02")}
	t, err := buildTemplate("content/analytics.html")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(nil)
		return
	}
	err = t.ExecuteTemplate(w, "layout", dd)
	if err != nil {
		fmt.Println(err.Error())
	}
}

type dashData struct {
	Date         string
	SessionCount int
	URLHits      map[string]map[string]int
}

func buildTemplate(templateName string) (*template.Template, error) {
	return template.New("").ParseFiles(templateName)
}

type fileData struct {
	Date    string
	Entries map[string][]action
}

type action struct {
	Page  string
	Query string
}

type analytics struct {
	Name      string
	IPEntries map[string]map[string][]action
	Password  string
	URLData   func(URL string) (groupPart, entryPart string)
	Mux       *sync.RWMutex
}

func (a analytics) readSavedData(td time.Time) map[string][]action {
	fileName := a.Name + "-analytics-" + td.Format("2006-01-02")
	entries := map[string][]action{}
	if _, err := os.Stat(fileName); os.IsNotExist(err) {

	} else {
		bytes, err := ioutil.ReadFile(fileName)
		if err != nil {
			fmt.Println(err.Error())
			return entries
		}
		err = json.Unmarshal(bytes, &entries)
		if err != nil {
			fmt.Println(err.Error())
		}
	}
	return entries
}

func (a analytics) insert(ip string, act action) {
	ts := time.Now().Format("2006-01-02")
	stamps := a.IPEntries[ts]
	if stamps == nil {
		a.IPEntries[ts] = map[string][]action{}
	}
	entries := stamps[ip]
	if entries == nil {
		entries = []action{}
	}
	entries = append(entries, act)

	a.IPEntries[ts][ip] = entries
}

func (a analytics) get() map[string]map[string][]action {
	return a.IPEntries
}

func (a analytics) getByIP(ip string) []action {
	return a.IPEntries[time.Now().Format("2006-01-02")][ip]
}

func (a analytics) writeFile() error {
	a.Mux.Lock()
	defer a.Mux.Unlock()
	for k, e := range a.IPEntries {
		data, err := json.Marshal(e)
		if err != nil {
			return err
		}
		f, err := os.Create(a.Name + "-analytics-" + k)
		if err != nil {
			return err
		}
		defer f.Close()
		_, err = f.Write(data)
		if err != nil {
			return err
		}
	}
	if len(a.IPEntries) > 1 {
		a.IPEntries = map[string]map[string][]action{}
		a.IPEntries[time.Now().Local().Format("2006-01-02")] = a.readSavedData(time.Now().Local())
	}
	return nil
}
